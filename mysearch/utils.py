# -*- coding: utf-8 -*-

import os
from urllib2 import urlparse

import requests

#: The lists. Read the files only at startup, or in time intervals.
blacklist = None
promotelist = None
replacelist = None

#: Placeholder to replace with search keywords
KEYWORDS = '{KEYWORDS}'

BLACKLIST_FILE = "blacklist.txt"
PROMOTELIST_FILE = "promotelist.txt"
REPLACELIST_FILE = "replacelist.txt"

#: Caching:
LIST_FILES = {}  # cache lists' content.
REMOTE_LIST_ITEM = {}  # Remember remote lists, only one http request for them.

def _remove_comments(ll):
    """
    Remove comments or null strings.
    - ll: list of str
    """
    return [it.strip() for it in ll if not (it.startswith('#') or not it)]

def _remove_domain(ll):
    """Just remove "www'" from a domain name, so we are not obliged to
    write it in our lists.
    - ll: list of domains, 'www.rst.ldv.uie'
    """
    return [it.strip('www.') for it in ll]

def read_blacklist(blacklist_file=BLACKLIST_FILE):
    """
    Reads ...
    - return: a list of sites (str)
    """
    global blacklist
    if not blacklist:
        with open(blacklist_file, "rb") as f:
            blacklist= f.readlines()
            return blacklist

    return blacklist

def url_netloc(url):
    """
    From the result's url, return the netloc part, i.e. 'amazon.com'
    """
    url_parsed = urlparse.urlparse(url)
    netloc = url_parsed[1].strip('www.') # strip, just in case
    return netloc

def blacklist_if_needed(result, blacklist=None):
    if not blacklist:
        blacklist = read_blacklist()

    # rm trailing newlines and 'www.', so we can either write them in the blacklist or not.
    blacklist = _remove_comments(blacklist)
    blacklist = [it for it in blacklist if it]
    blacklist = _remove_domain(blacklist)
    netloc = url_netloc(result.link_url)

    if netloc in blacklist:
        result.blacklisted = True

    return result

def _insert_list_inplace(target, to_add, i):
    """Insert all the elements of list to_add at the place of the list target[i].
    """
    del target[i]
    for j, it in enumerate(to_add):
        target.insert(i+j, it)

    return target

def read_listfile(fname):
    """Read filename fname. Read it once and cache its content. A list
    item can ask to fetch a remote list.

    Return: a list of str.
    """
    if fname in LIST_FILES:
        return LIST_FILES[fname]

    replacelist = []
    if os.path.exists(fname):
        with open(fname, 'r') as f:
            replacelist = f.readlines()
        LIST_FILES[fname] = replacelist

    return replacelist



def get_remote_list_item(item):
    """Get remote lists. Cache the http call.

    - Return: a list of lines.
    """
    # Could re-read regularly, so we wouldn't stop the server to load
    # other lists. There is requests-cache.
    if item in REMOTE_LIST_ITEM:
        return REMOTE_LIST_ITEM[item]

    res = []
    try:
        res = requests.get(item)
        res = res.content.splitlines()
        res = _remove_comments(res)
        REMOTE_LIST_ITEM[item] = res
    except Exception as e:
        print e

    return res

def read_list(replacelist):
    """Read the promote and the replace list (they have a similar format).
    Need to read the file first.
    Read remote lists: line starts with http and ends with .txt.

    - replacelist: list of str

    Return: a list of strings.
    """
    replacelist = _remove_comments(replacelist)
    replacelist = [it for it in replacelist if it]

    # Get a remote list.
    for i, item in enumerate(replacelist):
        if item.startswith('http') and item.endswith('txt'):
            to_add = get_remote_list_item(item)
            # Insert the link content in place of the url.
            replacelist = _insert_list_inplace(replacelist, to_add, i)

    return replacelist

def replace_if_needed(result, keywords, replacelist=None):
    """
    - result: backends.TxtResult

    Return: result
    """
    if not replacelist:
        replacelist = read_listfile(REPLACELIST_FILE)
        replacelist = read_list(replacelist)

    for line in replacelist:
        source, target = line.split(' ')
        netloc = url_netloc(result.link_url)

        if netloc in source:
            result.link_url = target.replace(KEYWORDS, keywords)
            result.link_name = result.link_name.lower()
            result.link_name = result.link_name.replace(netloc, url_netloc(target))
            result.link_resume = u"Un lien vers {} a été remplacé par {}".format(source, url_netloc(target))
            result.redirect_link_url = target.replace(KEYWORDS, keywords)
            return result

    return result

def promote(result, keywords, promotelist=None):
    """
    Promote a website next to a result.
    """
    if not promotelist:
        promotelist = read_listfile(PROMOTELIST_FILE)
        promotelist = read_list(promotelist)

    for line in promotelist:
        source, target = line.split(' ')
        netloc = url_netloc(result.link_url)

        if netloc in source:
            result.promote_name = url_netloc(target)
            result.promote_url = target.replace(KEYWORDS, keywords)

    return result
