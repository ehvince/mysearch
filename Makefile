
dev:
	# Install dev libs.
	pip install -r mysearch/requirements-dev.txt

test:
	# -s: stop on breakpoints
	pytest -s

run:
	python mysearch/mysearch.py
