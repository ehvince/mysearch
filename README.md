# MySearch

Mysearch is a metasearch engine that acts as a proxy for search engines like Google, Youtube, Openstreetmap, etc...

It's designed to anonymate your search requests and have a better display of search results (no ads, no sponsored results, compact format)

An public instance is available at https://search.jesuislibre.net/

## Filtering search restults and suggesting others

### Promote list

Some websites appear high in the results, but we'd like to suggest
"better" ones, like free (libre) alternatives to proprietary software
and services (for example, OpenStreetMap).

This feature adds a small text and a link below the actual result.

### Replace list

Some websites have a strict equivalent that we may want to always show
in place of what google returns us.

This feature totally replaces one link to a site by a link to another
(for example, amazon.fr by lalibrairie.fr for books).

### Blacklist

The blacklisted website will not appear in your search results. Fed up of facebook links ? :)

### Using third-party lists

We can use third-party lists that are referenced somewhere else, by
just giving its url in one line. For example, this is valid:

    https://framagit.org/ehvince/mysearch-lists/raw/master/promotelist.txt

this list would be loaded once at the first request, then cached (so,
the first request can take some time, it will take longer that other
usual mysearch searches).


# License
Copyright (C) 2013   Tuxicoman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Requirements

Dependencies are:
- python 2.7
- python-twisted
- python-jinja2
- python-pyasn1-modules
- python-pyasn1

# Use

To launch the software, just do :
$ python mysearch.py
Then access the Search engine in your browser through http://localhost:60061/

You can edit settings in the mysearch.cfg file.

# Develop

To run mysearch:

    python mysearch/mysearch.py

To install the dependencies locally, create a virtualenv and run:

    pip install -e .

Install also the development libraries:

    make dev

Make sure you have the following Debian packages:

    apt-get install libffi-dev

# Integration with Apache webserver

You can expose the search through Apache webserver too. Put those lines a website.conf virtualhost Apache file:

	ProxyPass / http://localhost:60061/
	RequestHeader set X-App-Location https://mydomain.com
	RequestHeader set X-App-Scheme https


Enjoy !
